#define _POSIX_C_SOURCE 	199309L	/* nanosleep() */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <comedilib.h>

#define DEVPATH					"/dev/comedi0"

int main(int argc, char* argv[])
{
	comedi_t* comedi_dev;
	char* dev_path;
	int n_subdevices;
	int subdev_type;
	unsigned int outval = 0;
	int status;
	int ret = EXIT_SUCCESS;
	int subdev;

	if (argc < 2) {
		dev_path = DEVPATH;
		fprintf(stderr, "Comedi device not set, using default %s\n", DEVPATH);
	} else {
		dev_path = argv[1];
	}

	/* Open the device */
	comedi_dev = comedi_open(dev_path);
	if (comedi_dev == NULL) {
		comedi_perror("comedi_open");
		return EXIT_FAILURE;
	}

	/* Do not use hard-coded subdev id, try to enumerate it */
	n_subdevices = comedi_get_n_subdevices(comedi_dev);
	for (subdev = 0; subdev < n_subdevices; subdev++) {
		subdev_type = comedi_get_subdevice_type(comedi_dev, subdev);
		if (subdev_type == COMEDI_SUBD_DO) {
			ret = EXIT_SUCCESS;
			break;
		}
	}
	if (ret == EXIT_FAILURE) {
		fprintf(stderr, "Unable to find subdevice of type Digital Output\n");
		goto leave;
	}

	/* Test the dio_write functionality only once */
	status = comedi_dio_write(comedi_dev, subdev, 0, 0x0);
	if (status == -1) {
		comedi_perror("comedi_dio_write");
		ret = EXIT_FAILURE;
		goto leave;
	}

	while(1) {
		comedi_dio_write(comedi_dev, subdev, 0, outval & 1);
		outval = ~outval;

		printf(".");
		fflush(stdout);

		nanosleep((struct timespec[]){0, 500000000}, NULL);
	}

leave:
	comedi_close(comedi_dev);
	return ret;
}
