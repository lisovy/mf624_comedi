CC = gcc

all: dio

dio: dio.c
	$(CC) $< -o $@ -lcomedi -lm
clean:
	rm -f *.o dio
